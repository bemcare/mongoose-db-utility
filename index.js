const mongoose = require('mongoose');
const DBUtil = require('./DBUtil');

module.exports = new DBUtil(mongoose);