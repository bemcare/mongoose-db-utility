class DBUtil {
    constructor(mongoose) {
        this.mongoose = mongoose;
        this.connectionStatus = 'disconnected';
    }
    
    async connect(uri, options = {}) {
        options = {
            useNewUrlParser: true,
            useCreateIndex: true,
            autoIndex: false, 
            useUnifiedTopology: true,
            ...options
        };
    
        try {
            await this.mongoose.connect(uri, options);
        } catch (err) {
            console.error(`[express-mongoose] Failed to connect to mongo database - ${err.name} - ${err.message}`);
    
            throw err;
        }

        this.mongoose.connection.on('connected', () => { 
            this.connectionStatus = 'connected';
            console.log('[express-mongoose] Connection connected'); 
        });

        this.mongoose.connection.on('reconnected', () => {
            this.connectionStatus = 'connected';
            console.log('[express-mongoose] Connection reconnected'); 
        });

        this.mongoose.connection.on('error',function (err) { 
            console.error(`[express-mongoose] Error on connection - ${err.name} - ${err.message}`);
        }); 
        
        this.mongoose.connection.on('disconnected', function () { 
            this.connectionStatus = 'disconnected';
            console.log('[express-mongoose] Connection disconnected'); 
        });        
    }

    on(event, callback) {
        return this.mongoose.connection.on(event, callback);
    }
}

module.exports = DBUtil;