const chai = require('chai');
const expect = chai.expect;

const DBUtil = require('../DBUtil');

const successTestDBUri = process.env.MONGO_URI;
const errorTestDBURI = 'mongodb+srv://test:test@cluster0-exon7.mongodb.net/test?retryWrites=true&w=majority';


const mongoose = require('mongoose');

describe('test DB class', function() {
    it('should connect to database', async () => {
        let _err = null;
        const db = new DBUtil(mongoose);

        try {
            await db.connect(successTestDBUri);
        } catch (err) {
            _err = err;
        }

        expect(_err).to.be.equal(null);
    });

    it ('should not connected to database', async() => {
        const start = new Date();
        const db = new DBUtil(mongoose);

        let errTime = null;

        try {
            await db.connect(errorTestDBURI, {serverSelectionTimeoutMS: 5000});
        } catch (err) {
            errTime = new Date();
        }

        expect(errTime).to.not.be.equal(null);
        expect(errTime.getTime() - start.getTime()).to.be.lessThan(6000);
    });
});
